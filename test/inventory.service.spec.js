(function() {
  'use strict';

  let expect = chai.expect;

  describe('inventory service', function(){

    let InventoryService;

      beforeEach(module('inventory')); // this is your ng-app

      beforeEach(inject(function(_InventoryService_){ // inject the thing mathching the name of the argument
        InventoryService = _InventoryService_;
      }));

      afterEach(function() {
       localStorage.removeItem('items');
     });

    it('should add 1 + 1', function(){
        expect(1+1).to.equal(2);
    });

    it('should handle empty object', function() {
      InventoryService.addItemToInventory({});
      let result = InventoryService.getAllItems();
      expect(result.length).to.equal(0);
    });

    it('should provide us with an array of objects', function() {
            let result = InventoryService.getAllItems();
            expect(result).to.be.an('array');
        });

  });

}());
