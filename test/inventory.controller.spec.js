(function() {
  'use strict';

  let expect = chai.expect;

  describe('InventoryController', function() {
    let InventoryController;
    let scope;
    let mockInventoryService = {};

    beforeEach(module('inventory'));

    beforeEach(module(function($provide) {
      $provide.value('InventoryService', mockInventoryService);
    }));

    beforeEach(inject(function($controller) {
      mockInventoryService.getAllItems = function getAllItems() {
        return [];
      };
      mockInventoryService.addItemToInventory = function addItemToInventory() {

      };
      InventoryController = $controller('InventoryController',{
        $scope: scope,
        InventoryService: mockInventoryService
      });
    }));

    it('should be an array', function() {
     expect(InventoryController.inventoryList).to.be.an('array');
   });

   it('should be an object', function(){
     expect(InventoryController.newItem).to.be.an('object');
});

it('should return false as a value', function() {
      expect(InventoryController.sortReverse).to.be.equal(false);
   });

 });

}());
