(function() {
  'use strict';

  angular.module('inventory')
  .controller('InventoryController', InventoryController);

  InventoryController.$inject = ['$scope','InventoryService'];

  function InventoryController($scope, InventoryService) {
    let self = this;
    self.newItem     = {}; // store the incoming item information 
    self.sortType    = 'price'; //sets the default sort type
    self.sortReverse = false; //sets the default sort order
    self.inventoryList = InventoryService.getAllItems();

   /**
     * Gets the new price of items through subtraction of discounted item from original price then
     * adding the tax by multiplying the result by 1.0575
     * @param {Object} takes in single item from list
     * @return {Number} Final price
     */
    self.getNewPrice = function getNewPrice(item) {
      let discountPrice = item.price - item.discount;
      let finalPrice = discountPrice * 1.0575;
      return finalPrice;
    };

    self.changeSort = function changeSort(sortField){
      console.log("sorting with field :", sortField);
      self.sortType = sortField;
      self.sortReverse = !self.sortReverse;
    };

    self.addItem = function addItem(item) {
      console.log("Item is ", item);
      InventoryService.addItemToInventory(item);
      // self.newItem = {}; // clears the input fields
    };
  }



}());
